﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace Practica1_Sergio
{
    class Program
    {
        static void Main(string[] args)
        {
            ejercicio7();
        }
        public static void ejercicio1()
        {
            //introducir 10 notas y decir aprobados y sobresalientes
            int aprobados, total, sobresaliente;
            aprobados = 0;
            total = 0;
            sobresaliente = 0;
            double[] nota = new double[10];
            while (total < 10)
            {
                Console.WriteLine("introduzca nota");
                nota[total] = double.Parse(Console.ReadLine());
                if (nota[total] >= 5)
                {
                    aprobados++;
                    if (nota[total] >= 9)
                    {
                        sobresaliente++;
                    }
                }
                total++;
            }
            Console.WriteLine("el numero de aprobados es " + aprobados + " de los cuales " + sobresaliente + " son sobresalientes");
        }
        public static void ejercicio2()
        {
            //introducir fecha de ultima facturacion y devuelve la fecha de proxima facturacion teniendo en cuenta que todos los meses tienen 30 dias.
            string fecha1;
            string dia;
            string ano;
            string mes;
            int mes2;
            int ano2;
            Console.WriteLine("introduzca fecha de ultima facturacion dd/mm/aaaa");
            fecha1 = Console.ReadLine();
            mes = fecha1.Substring(3, 2);
            dia = fecha1.Substring(0, 2);
            ano = fecha1.Substring(6, 4);
            mes2 = Int32.Parse(mes);
            ano2 = Int32.Parse(ano);
            mes2 = mes2 + 3;
            if (mes2==13)
            {
                mes2 = 1;
                ano2++;
            }
            if (mes2==14)
            {
                mes2 = 2;
                ano2++;
            }
            if (mes2==15)
            {
                mes2 = 3;
                ano2++;
            }
            Console.WriteLine("la proxima factura le llegara el dia " + dia + "/" + mes2 + "/" + ano2);

        }
        public static void ejercicio3()
        {
            //dar 10 numeros y el programa los leera al derecho (bucle for) y al reves (while)
            int[] num = new int[10];
            string dcha, izq;
            dcha = "";
            izq = "";
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("introduzca numero");
                num[i] = Int32.Parse(Console.ReadLine());
            }
            for (int i = 0; i < 10; i++)
            {
                dcha = dcha + " " + num[i];
            }
            Console.WriteLine(dcha);
            int cont;
            cont = 9;
            while (cont>=0)
            {
                izq = izq + " " + num[cont];
                cont = cont - 1;
            }
            Console.WriteLine(izq);
        }
        public static void ejercicio4()
        {
            //introducir ganancias y perdidas de 10 empresas y devolver nombre de la peor empresa
            double[] ganancias = new double[10];
            double[] perdidas = new double[10];
            double[] total = new double[10];
            double mala = 10000000;
            int peor=0;
            string[] empresa = new string[10];
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("introducir ganancias empresa " + (i+1));
                ganancias[i] = double.Parse(Console.ReadLine());
                Console.WriteLine("introducir perdidas empresa " + (i+1));
                perdidas[i] = double.Parse(Console.ReadLine());
                total[i] = ganancias[i] - perdidas[i];
                empresa[i] = "Empresa" + (i+1) + " con " + total[i] + "$ de beneficio";
            }
            for (int i = 0; i < 10; i++)
            {
                mala = Math.Min(mala, total[i]);
            }
            for (int i = 0; i < 10; i++)
            {
                if (total[i]==mala)
                {
                    peor = i;
                }
            }
            Console.WriteLine("la peor empresa ha sido la " + empresa[peor]);
        }
        public static void ejercicio5()
        {
            //tomar 10 numeros e imprimir los dos mayores
            double[] num = new double[10];
            double max, max2;
            max2 = -1000;
            max = -1000;
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("introduce numero");
                num[i] = double.Parse(Console.ReadLine());
                max = Math.Max(max, num[i]);
            }
            for (int i = 0; i < 10; i++)
            {
                if (num[i] != max)
                {
                    max2 = Math.Max(max2, num[i]);
                }
                
            }
            Console.WriteLine("el numero mas alto es " + max + " y el segundo " + max2);
        }
        public static void ejercicio6()
        {
            //introduce 10 numeros y dice cuantos numero distintos hay
            int cont = 0;
            int contT = 1;
            int[] num = new int[10];
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("introduce numero");
                num[i] = Int32.Parse(Console.ReadLine());
            }
            for (int i = 0; i < 10; i++)
            {
                cont = 0;
                if (num[i] == num[0])
                {
                    cont++;
                }
                if (num[i] == num[1])
                {
                    cont++;
                }
                if (num[i] == num[2])
                {
                    cont++;
                }
                if (num[i] == num[3])
                {
                    cont++;
                }
                if (num[i] == num[4])
                {
                    cont++;
                }
                if (num[i] == num[5])
                {
                    cont++;
                }
                if (num[i] == num[6])
                {
                    cont++;
                }
                if (num[i] == num[7])
                {
                    cont++;
                }
                if (num[i] == num[8])
                {
                    cont++;
                }
                if (num[i] == num[9])
                {
                    cont++;
                }
                if (cont==1)
                {
                    contT++;
                }
            }
            Console.WriteLine("hay " + contT + " numeros distintos");
        }
        public static void ejercicio7()
        {
            //leer 10 numero enteros y hallar media, si mayor-menor>3 no tenerlos en cuenta
            int[] num = new int[10];
            int max, min;
            double media;
            max = -1000;
            min = 1000;

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("introduzca numero");
                num[i] = Int32.Parse(Console.ReadLine());
                max = Math.Max(max, num[i]);
                min = Math.Min(min, num[i]);
            }
            if ((max-min)>=3)
            {
                media = (num[0] + num[1] + num[2] + num[3] + num[4] + num[5] + num[6] + num[7] + num[8] + num[9] - max - min) / 8;
            }
            else
            {
                media = (num[0] + num[1] + num[2] + num[3] + num[4] + num[5] + num[6] + num[7] + num[8] + num[9]) / 10;
            }
            Console.WriteLine("la media es " + media);
        }
    }
}
